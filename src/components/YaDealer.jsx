import React, { useState } from "react";

const YaDealer = () => {
    // 입력한 예산 관리
    const [userPrice, setUserPrice] = useState('')
    // 추천 할 차 이름 관리
    const [closestCar, setClosestCar] = useState('')
    // 에러 관리
    const [error, setError] = useState('')

    // 차 카탈로그
    const cars = [
        { name: "캐스퍼", price: 1200 },
        { name: "소나타", price: 2000 },
        { name: "k5", price: 3000 },
        { name: "g70", price: 4300 },
        { name: "사이버트럭", price: 8000 },
    ]

    // 입력값 변화 핸들링
    // 이벤트를 통해 자동으로 전달 됨
    // 원웨이 바인딩이기 때문에 해줌
    const handleUserPriceChange = (e) => {
        setUserPrice(e.target.value)
        setClosestCar('')
        setError('')
    }

    // 로직 어떻게 짜지?
    // const calculateCar = () => {
    //     // 가용 가능한 금액 = 예산 - 고정금액
    //     // 보험료 300 공채 30 탁송 40
    //     // 취득세율 7% 계산
    //     const availableuserPrice = Math.floor((userPrice - 370) / 1.07)
    //     let closestCar = cars[0]
    //
    //     for (let i = 0; i <= cars.length; i++) {
    //         if (availableuserPrice >= cars[i].price) {
    //             closestCar = cars[i]
    //             setClosestCar(closestCar.name)
    //         }
    //         else setError("구매 할 수 있는 차량이 없습니다.")
    //     }
    // }

    const calculateCar = () => {
        const adjustedPrice = (userPrice - 370) / 1.07
        let closestCar = cars[0]
        let minDiff = Math.abs(cars[0].price - adjustedPrice)

        for (let i = 1; i < cars.length; i++) {
            const diff = Math.abs(cars[i].price - adjustedPrice)
            if (diff < minDiff && cars[i].price <= adjustedPrice) {
                minDiff = diff
                closestCar = cars[i]
            }
        }

        if (closestCar.price > userPrice) {
            setError('돈이 부족하여 살 수 없습니다.')
        } else {
            setClosestCar(closestCar.name)
        }
    }

    // JSX 어떻게 그리지?
    // onChange 상황이랑 값이 다 전달 됨
    // onClick 으로 로직 돌림
    return (
        <div>
            <h2>야딜러</h2>
            <h3>가지고 있는 현금 다 내놔</h3>
            <p>(단위: 만원)</p>
            <input type="number" name="userPrice" value={userPrice} onChange={handleUserPriceChange}/>
            <button onClick={calculateCar}>화긴</button>
            {error ? <p>{error}</p> : <p>추천 차량: {closestCar}</p>}
        </div>
    )
}

export default YaDealer