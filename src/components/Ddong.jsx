import React, { useState } from "react";

const Ddong = () => {
    const [currentQuestion, setCurrentQuestion] = useState(0)
    const [score, setScore] = useState(0)

    // 왜 만들고 있지? 목적
    // 사람들한테 문제 한번 풀어보라고 경각심 일깨워주고 점수 합산해서 병원 오라고 꼬실라고
    const questions = [
        {
            question: "최근 언제 똥을 쌌나요?",
            answers: [
                { text: "3일 이내", score: 10 },
                { text: "1주일 이내", score: 5 },
                { text: "그 외", score: 1 },
            ],
        },
        {
            question: "최근 혈변을 했나요?",
            answers: [
                { text: "1개월 이내", score: 1 },
                { text: "6개월 이내", score: 5 },
                { text: "1년 이내 or 하지 않았음", score: 10 },
            ],
        },
        {
            question: "최근 설사를 했나요?",
            answers: [
                { text: "1개월 이내", score: 1 },
                { text: "6개월 이내", score: 5 },
                { text: "1년 이내 or 하지 않았음", score: 10 },
            ],
        },
        {
            question: "최근 잔변감이 있었나요?",
            answers: [
                { text: "1개월 이내", score: 1 },
                { text: "6개월 이내", score: 5 },
                { text: "1년 이내 or 하지 않았음", score: 10 },
            ],
        },
        {
            question: "최근 통증이 있었나요?",
            answers: [
                { text: "1개월 이내", score: 1 },
                { text: "6개월 이내", score: 5 },
                { text: "1년 이내 or 하지 않았음", score: 10 },
            ],
        },
        {
            question: "최근 똥의 모양은 선택 해주세요.",
            answers: [
                { text: "물", score: 1 },
                { text: "토끼", score: 3 },
                { text: "진흙", score: 5 },
                { text: "말", score: 8 },
                { text: "바나나", score: 10 },
            ],
        },
        {
            question: "과거에 같은 경험이 있나요?",
            answers: [
                { text: "1개월 이내", score: 1 },
                { text: "6개월 이내", score: 5 },
                { text: "1년 이내 or 하지 않았음", score: 10 },
            ],
        },
    ]

    // 결과에 대한 함수
    const getResult = score => {
        if (score >= 50) {
            return "당신은 장이 매우 건강합니다."
        } else {
            return "당신은 장이 조금 안 좋아요. 병원으로 오세요."
        }
    }

    // 점수 핸들링 메서드
    const handleAnswerClick = answerScore => {
        setScore(score + answerScore)
        const nextQuestion = currentQuestion + 1
        if (nextQuestion < questions.length) {
            setCurrentQuestion(nextQuestion)
        } else {
            alert(getResult(score + answerScore))
        }
    }

    return (
        <div>
            <h2 className="questionTitle">{questions[currentQuestion].question}</h2>
            {questions[currentQuestion].answers.map((answer) => (
                <div className="allButton">
                <button className="questionButton" key={answer.text} onClick={() => handleAnswerClick(answer.score)}>
                    {answer.text}
                </button>
                </div>
            ))}
        </div>
    );
};

export default Ddong;